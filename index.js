const candidatos = [
  "Fran",
  "German",
  "Adri",
  "Rober",
  "Rami",
  "Nahuel",
  "Hernan",
];
let moderadoresSeleccionados = [];
const mensajeErrorInput = `El número ingresado debe ser entre 1 y ${candidatos.length}`;

const jsAnunciarModeradores = () => {
  if (esCantidadModeradoresValida()) {
    escribirRespuestaEnPagina(
      mensajeModeradorSeleccionado(seleccionarModeradores())
    );
  } else {
    escribirRespuestaEnPagina(mensajeErrorInput);
  }
};

const esCantidadModeradoresValida = () =>
  cantidadModeradores() > 0 && cantidadModeradores() <= candidatos.length;

const cantidadModeradores = () =>
  document.getElementById("js-cantidad-moderadores").value;

const escribirRespuestaEnPagina = (mensaje) => {
  document.getElementById("js-respuesta").innerHTML = mensaje;
};

const mensajeModeradorSeleccionado = (moderador) => {
  return moderador.map(
    (moderadorSeleccionado) => `
        <div class="row">
            <div class="col s12 m4">
                <div class="card">
                    <div class="card-content center">
                      <p id="{moderador-${moderadorSeleccionado}"}>${moderadorSeleccionado}</p>
                    </div>
                    <div class="card-action center">
                      <i id="js-reseleccionar-moderador" 
                         class="material-icons center js-reseleccionar-moderador-${moderadorSeleccionado}">close</i>
                    </div>
                </div>
            </div>
        </div>
`
  );
};

const seleccionarModeradores = () => {
  moderadoresSeleccionados = [];
  while (moderadoresSeleccionados.length != cantidadModeradores()) {
    seleccionarModeradoresSinRepetidos();
  }
  return moderadoresSeleccionados;
};

const seleccionarModeradoresSinRepetidos = () => {
  let moderador = seleccionarModeradorAleatoriamente();
  if (!moderadoresSeleccionados.includes(moderador)) {
    moderadoresSeleccionados.push(moderador);
  } else {
    return seleccionarModeradoresSinRepetidos();
  }
};

const seleccionarModeradorAleatoriamente = () => {
  const indice = Math.floor(Math.random() * candidatos.length);
  return candidatos[indice];
};

const jsReseleccionarModerador = (event) => {
  seleccionarModeradoresSinRepetidos();
  const nombreModeradorSeleccionado = obtenerNombreModeradorSeleccionado(event);
  moderadoresSeleccionados = moderadoresSeleccionados.filter(
    (nombreModerador) => nombreModerador !== nombreModeradorSeleccionado
  );
  escribirRespuestaEnPagina(mensajeModeradorSeleccionado(moderadoresSeleccionados));

};

const obtenerNombreModeradorSeleccionado = (e) => e.target.className.split(" ")[2].split("-")[3];

document
  .getElementById("js-seleccionar-moderadores")
  .addEventListener("click", jsAnunciarModeradores);

document.addEventListener("click", function (e) {
  if (e.target && e.target.id == "js-reseleccionar-moderador") {
    jsReseleccionarModerador(e);
  }
});
